gulp = require('gulp')
cssImage = require('gulp-css-image')
gPath = require('../gulp-paths.json')
flags = gPath.img.flags

gulp.task('flagCss', ->
    gulp.src(flags)
    .pipe(cssImage(
      css: false,
      scss: true,
      retina: true,
      root: "../images/flags"
      separator: "_",
      prefix: "flag_",
      name: "_flags.scss"
    ))
    .pipe(gulp.dest('./assets/styles/flags'))
  )
