gulp = require('gulp')
sass = require('gulp-sass')
sourcemaps = require('gulp-sourcemaps')
gPath = require('../gulp-paths.json')

gulp.task('sass', ->
  gulp.src('./assets/styles/main.scss')
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(gulp.dest('./dist/styles'))
  )
