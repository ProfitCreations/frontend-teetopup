gulp = require('gulp')
imageResize = require('gulp-image-resize')

gulp.task('flags', ->
    gulp.src('./assets/images/flags')
    .pipe(imageResize(
      width: 250,
      height: 140,
      crop: true,
      upscale: false
      ))
      .pipe(gulp.dest('./dist/images/flags'))
  )
